#include "../CircularBuffer.h"
#include <unistd.h>
#include <thread>

using namespace std;

void writingThread(CircularBuffer<int> *b)
{
    for(int i = 0; i<20; i++)
    {
        int num = rand();
        int r1 = b->write(&num);
        cout << "write in " << r1 << "\t";
        cout << "number= " << num << endl;
        sleep(rand()%5);
    }
}

void readingThread(CircularBuffer<int> *b)
{
    int i = 0;
    while(i<19)
    {
        sleep(rand()%5);
        int r1 = b->availableData();
        if(r1>0)
        {
            for(int j=0; j<r1; j++)
            {
                i++;
                int num = 0;
                r1 = b->availableData();
                int r3 = b->read(&num);

                cout << "available = " << r1 << "\t";
                cout << "read from " << r3 << "\t";
                cout << "number= " << num << endl;

            }
        }
        
    }
}

int main()
{
    CircularBuffer<int> b(10);
    
    thread _writingThread(writingThread, &b);
    thread _readingThread(readingThread, &b);

    _writingThread.join();
    _readingThread.join();
    
    return 0;
}
