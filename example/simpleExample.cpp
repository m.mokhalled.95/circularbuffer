#include "../CircularBuffer.h"
#include <unistd.h>
#include <thread>

using namespace std;

int main()
{

    CircularBuffer<int> b(20);
    while(1)
    {
        int r1 = b.availableData();
        int val = rand();
        int r2 = b.write(&val);
        int num = 0;
        int r3 = b.read(&num);

        cout << "avail= " << r1 << "\t";
        cout << "write= " << r2 << "\t";
        cout << "read= " << r3 << "\t";
        cout << "number= " << num << endl;
        sleep(1);
    }
    
    return 0;
}
