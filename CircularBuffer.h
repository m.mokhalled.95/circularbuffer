#ifndef __CIRCULAR_BUFFER__
#define __CIRCULAR_BUFFER__

#include <iostream>
#include <vector>
#include <stdint.h>
#include <stdlib.h>
#include <array>
#include <thread>
#include <mutex>

template <typename T>
class CircularBuffer
{
    private:
        //size of the buffer
        uint32_t size;  

        //position of writing next element in the buffer
        uint32_t writePosition = 0;

        //position of reading next element in the buffer
        uint32_t readPosition = 0;

        //the vector which stores the buffer data
        std::vector<T> valueVector;

        //mutex for controling reading and writing in multi thread programs
        std::mutex lock;

    public:
        //construction function
        CircularBuffer(uint32_t bufferSize);

        //retuns number of data in the buffer which were written but not read
        uint32_t availableData();

        //write new thing in the buffer, it returns the position of written data in the vector
        int64_t write(T* input);
        
        //read data from the buffer, it returns the position of read data in the vector
        int64_t read(T* output); 
};

template <typename T>
CircularBuffer<T>::CircularBuffer(uint32_t bufferSize)
{
    size = bufferSize;
    T temp;
    for(uint32_t i = 0; i<size; i++)
    {
        valueVector.push_back(temp);
    }

    if (valueVector.size() != size)
    {
        throw("There is a problem in creating data buffer");
    }
};

template <typename T>        
uint32_t CircularBuffer<T>::availableData()
{
    int64_t length = 0;
    if (writePosition > readPosition)
        length = writePosition - readPosition;
    else if(writePosition < readPosition)
        length = (size - readPosition) + writePosition;
         
    return (uint32_t) length;
};

template <typename T>        
int64_t CircularBuffer<T>::write(T* input)
{
    int ret = -1;
    if (availableData() < (size-1)) 
    {
        lock.lock();  
        valueVector[writePosition] = *input;
        ret = writePosition;
        

        if (writePosition+1 < size)
        {
            writePosition++;
        }
        else
        {
            writePosition = 0;
        } 
        lock.unlock();
    }
    return ret;
};

template <typename T>
int64_t CircularBuffer<T>::read(T* output)
{
    int ret = -1;
    if (availableData() > 0) 
    {
        lock.lock();
        *output = valueVector[readPosition];
        ret = readPosition;

        if (readPosition+1 < size)
        {
            readPosition++;
        }
        else
        {
            readPosition = 0;
        }
        lock.unlock();
    }
    return ret;
};

#endif